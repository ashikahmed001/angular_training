﻿import { Component, Input, OnInit } from '@angular/core';
import {Rating} from './rating.model';


@Component({
  selector: 'rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class ratingComponent implements OnInit {
    title = 'app';

    @Input() Ratings:Rating;
    objRating:Rating;

  ngOnInit()
  {
    
    this.objRating=this.Ratings;
   
    this.objRating=this.getRatings(this.Ratings);
  }

 

  private getMaxRating(rating:Rating):number{
    var arrRating:number[] = this.pushRatingArray(rating);
    var maxRating:number;

    if(arrRating && arrRating.length >0)
    {
      maxRating= Math.max(...arrRating);
    }
    return maxRating;
    
  }

  private getRatings(ratings:Rating):Rating{
    
    var Ratings=ratings;
    var maxRating=this.getMaxRating(ratings);
    if(maxRating)
    {
      Ratings.one.percentage=((Ratings.one.value/maxRating)*100).toString();
      Ratings.two.percentage=((Ratings.two.value/maxRating)*100).toString();
      Ratings.three.percentage=((Ratings.three.value/maxRating)*100).toString();
      Ratings.four.percentage=((Ratings.four.value/maxRating)*100).toString();
      Ratings.five.percentage=((Ratings.five.value/maxRating)*100).toString();
    }
    Ratings.average=this.calculateAverage(ratings);
    Ratings.totalRatings=this.pushRatingArray(ratings).reduce((a,b)=>a+b,0).toString();
    return Ratings;
    
  }

  private calculateAverage(rating:Rating){
    var ratings={};
    
    if(rating.one && rating.one.value)
    {
      ratings['one']=rating.one.value *1;
    }
    if(rating.two && rating.two.value)
    {
      ratings['two']=rating.two.value *2;
    }
    if(rating.three && rating.three.value)
    {
      ratings['three']=rating.three.value *3;
    }
    if(rating.four && rating.four.value)
    {
      ratings['four']=rating.four.value *4;
    }
    if(rating.five && rating.five.value)
    {
      ratings['five']=rating.five.value *5;
    }

    var totalStars=(ratings['one'])+ratings['two']+ratings['three']+ratings['four']+ratings['five'];
    var totalCount= this.pushRatingArray(rating).reduce((a,b)=>a+b,0);
    var average=(totalStars/totalCount).toFixed(1);

    return average;
  }

  private pushRatingArray(rating:Rating):number[]{
    var arrRating:number[]=[];
    if(rating)
    {
      if(rating.one && rating.one.value)
      {
        arrRating.push(rating.one.value)
      }
      if(rating.two && rating.two.value)
      {
        arrRating.push(rating.two.value)
      }
      if(rating.three && rating.three.value)
      {
        arrRating.push(rating.three.value)
      }
      if(rating.four && rating.four.value)
      {
        arrRating.push(rating.four.value)
      }
      if(rating.five && rating.five.value)
      {
        arrRating.push(rating.five.value)
      }
    }
    return arrRating;
  }
}
