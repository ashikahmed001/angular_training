import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ratingComponent } from './rating.component';
import {ratingDirective} from './rating.directive';
import {ratingPipe} from './rating.pipe';

@NgModule({
  declarations: [
    ratingComponent,
    ratingDirective,
    ratingPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [ratingComponent, ratingDirective, ratingPipe]
})
export class ratingModule { }
