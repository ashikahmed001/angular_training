import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name:'rater'
})
export class ratingPipe implements PipeTransform{

    transform(value:string){
        return this.CreateLabel(value);
    }

    CreateLabel(name:string):string
    {
      var alpha=this.getFirstValue(name);
      return alpha && alpha[0] ? alpha[0].charAt(0) + (alpha[0] && alpha[1] ? alpha[1].charAt(0):""):"N";
  
    }
  
    getFirstValue(value:string)
    { 
      var g=value.split(' ', 2);
      return g;
    }
}