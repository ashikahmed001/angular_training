export interface Rating{
    one:rater
    two:rater
    three:rater
    four:rater
    five:rater
    average?:string
    totalRatings:string
    totalReviews:string
}

interface rater{
    color:string
    value:number
    percentage?:string
}