import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {alphapicModule} from '../alphapic/alphapic.module'
import {ratingModule} from '../rating/rating.module'
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    alphapicModule,
    ratingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
