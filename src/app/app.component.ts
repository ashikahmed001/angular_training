import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  objRating:any;

  constructor(){
    this.objRating={
      one:{
        color:'red',
        value:246
      },
      two:{
        color:'orange',
        value:235
      },
      three:{
        color:'yellow',
        value:544
      },
      four:{
        color:'dodgerblue',
        value:123
      },
      five:{
        color:'lightgreen',
        value:421
      }

    }
  }
}
