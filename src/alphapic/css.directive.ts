import {Directive, ElementRef, Renderer2, AfterViewInit, HostListener} from '@angular/core';

@Directive({
    selector:'[cssStyler]'
})
export class cssDirective implements AfterViewInit{

    constructor(private element:ElementRef, private renderer:Renderer2){
    }

    ngAfterViewInit(){
        
    }

    @HostListener('mouseover') onMouserHover(){
        this.renderer.setStyle(this.element.nativeElement,'box-shadow','1px 2px 2px 1px #dadada');
        this.renderer.setStyle(this.element.nativeElement,'cursor','default');
    }

    @HostListener('mouseleave') onMouserLeft(){
        this.renderer.removeStyle(this.element.nativeElement,'box-shadow');
        this.renderer.removeStyle(this.element.nativeElement,'cursor');
    }

    
}