import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { alphapic } from './alphapic.component';
import {cssDirective} from './css.directive';
import {alphaPicPipe} from './alphapic.pipe';

@NgModule({
  declarations: [
    alphapic,
    cssDirective,
    alphaPicPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [alphapic, cssDirective, alphaPicPipe]
})
export class alphapicModule { }
